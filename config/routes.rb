Rails.application.routes.draw do
  resources :client_shares
  #resources :shares
  resources :visits, only: [:create, :index]
  resources :transactions, only: [:index]
  resources :accounts, only: [:show]
  devise_for :clients, controllers: { sessions: 'clients/sessions', registrations: 'clients/registrations' }, skip: [:registrations]


  devise_scope :client do
    root to: "clients/sessions#new"
    post '/account_register' => 'clients/registrations#create'
    get "/open_account" => "clients/registrations#new", as: "new_account_registration"
  end
  
   

  get '/my_account' => 'accounts#show_account'
  get '/account_list' => 'accounts#index'
  get '/my_transactions' => 'accounts#my_transactions'
  get '/account_transaction/:id' => 'accounts#account_transactions', as: 'account_transactions'
  get '/became_vip/:id' => 'accounts#became_vip', as: 'set_account_vip'
  get '/remove_vip/:id' => 'accounts#remove_vip', as: 'remove_account_vip'

  get '/deposit' => 'transactions#deposit'
  post '/execute_deposit' => 'transactions#execute_deposit'

  get '/withdraw' => 'transactions#withdraw'
  post '/execute_withdraw' => 'transactions#execute_withdraw'

  get '/transfer' => 'transactions#transfer'
  post '/execute_transfer' => 'transactions#execute_transfer'

  get '/shares' => 'shares#index'
  get '/my_shares' => 'shares#my_shares'
  post '/buy_shares' => 'shares#buy_shares'
  post '/sell_shares' => 'shares#sell_shares'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
