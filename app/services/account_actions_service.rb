class AccountActionsService

    attr_reader :client, :params, :account

    def initialize(client,params)
       @client = client 
       @params = params
       @account = @client.account
    end

    def deposit!        
        @account.transaction_desc = params[:description]
        @account.balance += params[:value].gsub(',','').to_f
        @account.save!
    end

    def withdraw
        value = params[:value].gsub(',','').to_f
        #return false if @account.balance-value < 0 && !@account.client.vip            
        @account.transaction_desc = params[:description]
        @account.balance -= value        
        @account.save
    end

    def transfer
        return 'Conta de origem e destino são as mesmas' if params[:target_account]==@client.account_number
        dest_client = Client.find_by_account_number(params[:target_account])        
        value = params[:value].gsub(',','').to_f        
        tax = @client.vip ? value*0.08 : 8.0
        valid_result = validate_transfer(value,tax,dest_client)
        return valid_result unless valid_result.blank?
        ActiveRecord::Base.transaction do
            @account.transaction_desc = params[:description]
            @account.balance -= value        
            @account.target_account = dest_client.account_number
            @account.save
            @account.balance -= tax  
            @account.transaction_desc = 'Taxa de Transferência'      
            @account.save
            dest_client.account.balance += value
            dest_client.account.transaction_desc = "Transferência Recebida"
            dest_client.account.target_account = @client.account_number 
            dest_client.account.save
        end  
        'Transferência concluída!'
    end

    def request_visit
        ActiveRecord::Base.transaction do
            visit = Visit.new
            visit.client = @client
            visit.save
            @account.balance -= 250
            @account.transaction_desc = 'Visita do gerente'
            @account.save
        end
    end

    def validate_transfer(value,tax,dest_client)
        return 'Conta inexistente' if dest_client.nil? || dest_client.account.nil?
        return 'Limite de transferência excedido' if !@client.vip && value > 1000
        return 'Saldo Insuficiente' if !@client.vip && @account.balance < (value+tax) 
    end
    

   

end
