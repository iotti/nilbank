class ShareService

    attr_reader :client, :params, :account

    def initialize(client,params)
        @client = client
        @params = params
        @account = client.account
    end

    def buy_share
        qtd = @params[:qtd].to_i
        buy_price =  @params[:buy_price].to_f
        total_price = buy_price*qtd
        return 'Saldo Insuficiente' if buy_price > @account.balance && !@client.is_vip?
        share = Share.find(@params[:share_id].to_i)
        ActiveRecord::Base.transaction do
            @account.balance -= total_price
            @account.transaction_desc = 'Compra de ' + @params[:qtd] + ' ação(ões) ' + share.name
            @account.save 
            cs = ClientShare.create client_id:@client.id, share_id: share.id, qtd: qtd, buy_price: buy_price            
        end
        'Compra efetuada!'
    end

    def sell_share
        c = ClientShare.find(@params[:id])        
        sell_price =  @params[:sell_price].to_f
        total_price = sell_price*c.qtd
        share = Share.find(c.share_id.to_i)
        ActiveRecord::Base.transaction do
            @account.balance += total_price
            @account.transaction_desc = 'Venda de ' + c.qtd.to_s + ' ação(ões) ' + share.name
            @account.save 
           c.destroy           
        end
        'Venda efetuada!'
    end
end