class Client < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,:registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :account
  has_many :transactions, through: :account
  has_many :client_shares

  after_create :create_account

  validates :name, presence: true, on: :create
  validates :account_number, length: { is: 5 }, on: :create, unless: Proc.new {is_manager?}

  after_create :default_values

  after_find :calculate_interests, unless: Proc.new {is_manager?}
  before_update :calculate_interests, unless: Proc.new {is_manager?}
  
  def default_values
    self.role_id ||= Role.find_by_name('Cliente').id
  end

  def email_required?
    false
  end
  
  def email_changed?
    false
  end

   def is_manager?
        role_id == Role.find_by_name('Gerente').id
   end

   private
   def create_account
     self.account ||= Account.create(balance:0,client_id:self.id) unless is_manager?
   end

   def calculate_interests
      if account.balance >= 0
        account.last_request = Time.now
        account.save
      else
        if account.last_request.nil?
          account.last_request = Time.now
          account.save
        end
        delta = (Time.now - account.last_request).floor / 60      

        if delta > 0
          account.balance += account.balance * 0.001 * delta
          account.last_request = Time.now
          account.transaction_desc = 'Juros'
          account.save
        end
      end
   end
end
