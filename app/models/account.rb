class Account < ActiveRecord::Base

  attr_accessor :transaction_desc, :target_account
  belongs_to :client
  has_many :transactions, -> {order 'created_at DESC'}
  
  validates :balance,numericality: {greater_than_or_equal_to: 0, message:'Saldo Insuficiente'}, on: :update, unless: Proc.new {self.client.vip}
  
  after_update :generate_transaction
  before_update :interest_date, if: Proc.new { |a| self.balance_was >= 0 }

  private
  def generate_transaction
    value = self.balance - self.balance_was
    return if value==0
    Transaction.create(value: value, account_id: self.id, description: transaction_desc, target_account: target_account)
  end

  def interest_date
    last_request = Time.now
  end


end
