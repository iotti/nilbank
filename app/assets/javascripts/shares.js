

function checkTime(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function refresh_time() {
  var today = new Date();
  var h = checkTime(today.getHours());
  var m = checkTime(today.getMinutes());
  var s = checkTime(today.getSeconds());
  document.getElementById('time_div').innerHTML = h + ":" + m + ":" + s;
  
}
