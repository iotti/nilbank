json.extract! transaction, :id, :value, :account_id, :description, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
