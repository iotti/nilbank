json.extract! visit, :id, :client_id, :created_at, :updated_at
json.url visit_url(visit, format: :json)
