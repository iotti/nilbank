class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_client!
  before_action :set_client_account, only: [:deposit, :withdraw, :transfer]
  before_action :client_only!

  before_action :build_transaction, only: [:deposit, :withdraw, :transfer]
  before_action :permit_transaction, only: [:execute_deposit, :execute_withdraw, :execute_transfer]
  # GET /transactions
  
  def deposit
    @is_transfer = false
    @action_name = 'Depósito'
    @account_action = execute_deposit_path
    render :account_action
  end

  def execute_deposit
    AccountActionsService.new(current_client,params[:transaction]).deposit!  
    redirect_to my_account_path, notice: 'Deposito efetuado!'
  end

  def withdraw
    @is_transfer = false
    @action_name = 'Retirada'
    @account_action = execute_withdraw_path
    render :account_action
  end

  def execute_withdraw      
    AccountActionsService.new(current_client,params[:transaction]).withdraw    
    redirect_to withdraw_path, notice:  current_client.account.errors.first[1]  and return if current_client.account.errors.any?
    redirect_to my_account_path, notcie: 'Retirada efetuada!' 
  end

  def transfer
    @is_transfer = true
    @action_name = 'Transferência'
    @account_action = execute_transfer_path
    render :account_action
  end

  def execute_transfer         
    redirect_to transfer_path, notice:  AccountActionsService.new(current_client,params[:transaction]).transfer    
  end

  private
    
    def permit_transaction
      params.require(:transaction).permit(:value,:description,:target_account)
    end

    def build_transaction
      @transaction = Transaction.new
    end
end
