class AccountsController < ApplicationController
  before_action :authenticate_client!
  before_action :authenticate_manager!, only: [:index,:show,:account_transactions, :became_vip, :remove_vip]
  before_action :set_account, only: [:show, :account_transactions, :became_vip, :remove_vip]
  
  before_action :client_only!, only: [:show_account,:my_transactions]
  before_action :set_client_account, only: [:show_account, :my_transactions]
  before_action :load_transactions, only: [:my_transactions, :account_transactions]
  before_action :load_last_transactions, only: [:show, :show_account] 


  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  def show_account
    
    render :show
  end

  def my_transactions
    render :transactions
  end

  def account_transactions
    render :transactions
  end

  def became_vip
    @account.client.vip = true
    @account.client.save
    redirect_to account_list_path
  end

  def remove_vip
    @account.client.vip = false
    @account.client.save
    redirect_to account_list_path
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
  end

    def load_transactions
      @transactions = @account.transactions
    end

    def load_last_transactions
      @transactions = @account.transactions.first(3)
    end

    
    

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:balance)
    end
end
