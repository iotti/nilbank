class ClientSharesController < ApplicationController
  before_action :set_client_share, only: [:show, :edit, :update, :destroy]

  # GET /client_shares
  # GET /client_shares.json
  def index
    @client_shares = ClientShare.all
  end

  # GET /client_shares/1
  # GET /client_shares/1.json
  def show
  end

  # GET /client_shares/new
  def new
    @client_share = ClientShare.new
  end

  # GET /client_shares/1/edit
  def edit
  end

  # POST /client_shares
  # POST /client_shares.json
  def create
    @client_share = ClientShare.new(client_share_params)

    respond_to do |format|
      if @client_share.save
        format.html { redirect_to @client_share, notice: 'Client share was successfully created.' }
        format.json { render :show, status: :created, location: @client_share }
      else
        format.html { render :new }
        format.json { render json: @client_share.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /client_shares/1
  # PATCH/PUT /client_shares/1.json
  def update
    respond_to do |format|
      if @client_share.update(client_share_params)
        format.html { redirect_to @client_share, notice: 'Client share was successfully updated.' }
        format.json { render :show, status: :ok, location: @client_share }
      else
        format.html { render :edit }
        format.json { render json: @client_share.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /client_shares/1
  # DELETE /client_shares/1.json
  def destroy
    @client_share.destroy
    respond_to do |format|
      format.html { redirect_to client_shares_url, notice: 'Client share was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_share
      @client_share = ClientShare.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_share_params
      params.require(:client_share).permit(:share_id, :client_id, :buy_price)
    end
end
