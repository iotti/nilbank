class SharesController < ApplicationController
  before_action :authenticate_client!
  before_action :client_only!, except: [:index]
  before_action :buy_shares_params, only: [:buy_shares]
  before_action :sell_shares_params, only: [:sell_shares]

  # GET /shares
  # GET /shares.json
  def index
    @shares = Share.all
  end

  def my_shares
    @shares = current_client.client_shares
    @share_list = ''
    shares = Share.all 
    shares.each do |s|
      @share_list += "%22#{s.name}%22#{s==shares.last ? '' : '%2C'}" 
    end
    
  end

  def buy_shares    
    redirect_to my_shares_path, notice: ShareService.new(current_client,params[:client_share]).buy_share
  end


  def sell_shares    
    redirect_to my_shares_path, notice: ShareService.new(current_client,params[:client_share]).sell_share
  end

 

  private
  def buy_shares_params
    params.require(:client_share).permit(:qtd,:share_id, :buy_price)
  end

  def sell_shares_params
    params.require(:client_share).permit(:id, :sell_price)
  end
    
end
