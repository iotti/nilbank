class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    added_attrs = [:password, :password_confirmation, :account_number, :vip, :name]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :sign_in, keys: [:account_number, :password, :remember_me]
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end

  def set_client_account
    @account = current_client.account
  end

  def authenticate_client!
    if client_signed_in?
      super
    else
      redirect_to login_path
    end
  end

  def after_sign_in_path_for(resource)
    current_client.is_manager? ? account_list_path : my_account_path
  end

  def authenticate_manager!
    redirect_to my_account_path unless current_client.present? && current_client.is_manager?
  end

  def vip_only!
    redirect_to(my_account_path, notice: 'Acesso restrito a clientes VIP') unless current_client.vip
  end

  def client_only!
    redirect_to account_list_path if current_client.present? && current_client.is_manager?
  end

 
end
