class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.decimal :value, :precision => 10, :scale => 2
      t.references :account, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
