class AddShares < ActiveRecord::Migration
  def change
    #https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quote%20where%20symbol%20in%20(%22PETR4.SA%22%2C%20%22VALE5.SA%22%2C%20%22BVMF3.SA%22%2C%20%22BRML3.SA%22%2C%20%22ALSC3.SA%22%2C%20%22MULT3.SA%22%2C%20%22BBAS3.SA%22%2C%20%22ITSA4.SA%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=
    
    Share.create(name:'PETR4.SA')
    Share.create(name:'VALE5.SA')
    Share.create(name:'BVMF3.SA')
    Share.create(name:'BRML3.SA')
    Share.create(name:'ALSC3.SA')
    Share.create(name:'MULT3.SA')
    Share.create(name:'BBAS3.SA')
    Share.create(name:'ITSA4.SA')
  end
end
