class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.decimal :balance, :precision => 10, :scale => 2
      t.references :client, index: true, foreign_key: true

      t.timestamps null: false
    end

  end
end
