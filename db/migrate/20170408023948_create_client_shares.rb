class CreateClientShares < ActiveRecord::Migration
  def change
    create_table :client_shares do |t|
      t.references :share, index: true, foreign_key: true
      t.references :client, index: true, foreign_key: true
      t.decimal :buy_price, :precision => 10, :scale => 2

      t.timestamps null: false
    end
  end
end
