class AddTargetAccountIdToTransactions < ActiveRecord::Migration
  def change
    add_column :transactions, :target_account, :string
  end
end
