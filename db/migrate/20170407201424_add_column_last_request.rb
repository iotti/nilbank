class AddColumnLastRequest < ActiveRecord::Migration
  def change
    add_column :accounts, :last_request, :timestamp
  end
end
