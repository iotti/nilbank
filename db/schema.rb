# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170410050705) do

  create_table "accounts", force: :cascade do |t|
    t.decimal  "balance",                precision: 10, scale: 2
    t.integer  "client_id",    limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.datetime "last_request"
  end

  add_index "accounts", ["client_id"], name: "index_accounts_on_client_id", using: :btree

  create_table "client_shares", force: :cascade do |t|
    t.integer  "share_id",   limit: 4
    t.integer  "client_id",  limit: 4
    t.decimal  "buy_price",            precision: 10
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "qtd",        limit: 4
  end

  add_index "client_shares", ["client_id"], name: "index_client_shares_on_client_id", using: :btree
  add_index "client_shares", ["share_id"], name: "index_client_shares_on_share_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.string   "encrypted_password",     limit: 255, default: "",    null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "role_id",                limit: 4,                   null: false
    t.string   "name",                   limit: 255,                 null: false
    t.string   "account_number",         limit: 255,                 null: false
    t.boolean  "vip",                                default: false, null: false
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
  end

  add_index "clients", ["account_number"], name: "index_clients_on_account_number", unique: true, using: :btree
  add_index "clients", ["reset_password_token"], name: "index_clients_on_reset_password_token", unique: true, using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "shares", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "transactions", force: :cascade do |t|
    t.decimal  "value",                      precision: 10, scale: 2
    t.integer  "account_id",     limit: 4
    t.string   "description",    limit: 255
    t.datetime "created_at",                                          null: false
    t.datetime "updated_at",                                          null: false
    t.string   "target_account", limit: 255
  end

  add_index "transactions", ["account_id"], name: "index_transactions_on_account_id", using: :btree

  create_table "visits", force: :cascade do |t|
    t.integer  "client_id",  limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "visits", ["client_id"], name: "index_visits_on_client_id", using: :btree

  add_foreign_key "accounts", "clients"
  add_foreign_key "client_shares", "clients"
  add_foreign_key "client_shares", "shares"
  add_foreign_key "transactions", "accounts"
  add_foreign_key "visits", "clients"
end
